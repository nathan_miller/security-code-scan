package main

import (
	"fmt"
	"os"
	"reflect"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/security-code-scan/v3/metadata"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/tmp/app")
}

func TestUniqueMatches(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name       string
		reportPath string
		expected   []string
	}{
		{
			name:       "single project",
			reportPath: "testdata/reports/single.txt",
			expected: []string{
				"WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]",
			},
		},
		{
			name:       "multi project",
			reportPath: "testdata/reports/multi.txt",
			expected: []string{
				"WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]",
				"WeakRandom.cs(8,17): warning SCS0007: Weak random generator [/tmp/app/app.csproj]",
				"WeakRandom.cs(9,18): warning SCS0008: Weak random generator [/tmp/app/app.csproj]",
			},
		},
		{
			name:       "long line",
			reportPath: "testdata/reports/large.txt",
			expected: []string{
				"file.cs(1,2): warning SCS0027: Open redirect [file.csproj]",
			},
		},
	}

	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			f, err := os.Open(tt.reportPath)
			if err != nil {
				t.Fatal(err)
			}
			got, err := uniqueScan(f)
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("Expected match to be\n%#v\nbut got\n%#v", tt.expected, got)
			}
		})
	}
}

func TestParse(t *testing.T) {
	want := &SecWarning{
		SourceFile:  "WeakRandom.cs",
		Line:        7,
		Column:      16,
		RuleID:      "SCS0005",
		Message:     "Weak random generator",
		ProjectFile: "/tmp/app/app.csproj",
	}
	got, _ := parse("WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]")
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Expected\n%#v\nbut got\n%#v", want, got)
	}
}

func TestConvert(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name       string
		reportPath string
		expected   *report.Report
	}{
		{
			name:       "single project",
			reportPath: "testdata/reports/single.txt",
			expected:   newVuln([]succinctVuln{{"SCS0005", 7, report.SeverityLevelHigh}}),
		},
		{
			name:       "multi project",
			reportPath: "testdata/reports/multi.txt",
			expected: newVuln([]succinctVuln{
				{"SCS0005", 7, report.SeverityLevelHigh},
				{"SCS0007", 8, report.SeverityLevelHigh},
				{"SCS0008", 9, report.SeverityLevelHigh},
			}),
		},
	}

	for _, tt := range tests {
		tt := tt // NOTE: https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			f, err := os.Open(tt.reportPath)
			if err != nil {
				t.Fatal(err)
			}

			got, err := convert(f, "app")
			if err != nil {
				t.Fatal(err)
			}
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", tt.expected, got)
			}
		})
	}
}

func TestFilepath(t *testing.T) {
	tests := []struct {
		name     string
		result   Result
		expected string
	}{
		{
			name: "csproj to sibling source",
			result: Result{
				SecWarning: &SecWarning{
					ProjectFile: "/tmp/project/app/app.csproj",
					SourceFile:  "WeakRandom.cs",
				},
				Root: "/tmp/project",
			},
			expected: "app/WeakRandom.cs",
		},
		{
			name: "csproj to nested source",
			result: Result{
				SecWarning: &SecWarning{
					ProjectFile: "/tmp/project/app.csproj",
					SourceFile:  "/tmp/project/subdir/src.cs",
				},
				Root: "/tmp/project",
			},
			expected: "subdir/src.cs",
		},
		{
			name: "csproj to sibling nested source",
			result: Result{
				SecWarning: &SecWarning{
					ProjectFile: "/tmp/project/WebGoat/WebGoat.NET.csproj",
					SourceFile:  "/tmp/project/WebGoat/WebGoatCoins/Orders.aspx.cs",
				},
				Root: "/tmp/project",
			},
			expected: "WebGoat/WebGoatCoins/Orders.aspx.cs",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.result.Filepath()
			if !reflect.DeepEqual(tt.expected, got) {
				t.Errorf("Expected match to be\n%#v\nbut got\n%#v", tt.expected, got)
			}
		})
	}
}

func NewResult(root string, projectFile string, sourceFile string) *Result {
	warning := &SecWarning{
		ProjectFile: projectFile,
		SourceFile:  sourceFile,
	}

	return &Result{
		SecWarning: warning,
		Root:       root,
	}
}

type succinctVuln struct {
	name     string
	line     int
	severity report.SeverityLevel
}

func newVuln(svulns []succinctVuln) *report.Report {
	vulns := make([]report.Vulnerability, len(svulns))

	for idx, succinctVuln := range svulns {
		vulns[idx] = report.Vulnerability{
			Category: "sast",
			Scanner: report.Scanner{
				ID:   "security_code_scan",
				Name: "Security Code Scan",
			},
			Severity: succinctVuln.severity,
			Name:     "Weak random generator",
			Message:  "Weak random generator",
			Location: report.Location{
				File:      "WeakRandom.cs",
				LineStart: succinctVuln.line,
			},
			Identifiers: []report.Identifier{
				{
					Type:  "security_code_scan_rule_id",
					Name:  succinctVuln.name,
					Value: succinctVuln.name,
					URL:   "https://security-code-scan.github.io/#" + succinctVuln.name,
				},
			},
			CompareKey: fmt.Sprintf("WeakRandom.cs:%d:%s", succinctVuln.line, succinctVuln.name),
		}
	}

	return &report.Report{
		Version:         report.CurrentVersion(),
		Vulnerabilities: vulns,
		DependencyFiles: []report.DependencyFile{},
		Analyzer:        metadata.AnalyzerName,
		Config:          ruleset.Config{Path: ruleset.PathSAST},
	}
}
